//
//  AppDelegate.h
//  PruebaEncriptado
//
//  Created by Adrián Zavala Coria on 17/02/15.
//  Copyright (c) 2015 Horus. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

