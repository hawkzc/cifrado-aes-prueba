//
//  NSData+NSData_AES256_m.h
//  PruebaEncriptado
//
//  Created by Adrián Zavala Coria on 17/02/15.
//  Copyright (c) 2015 Horus. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSData (NSData_AES256_m)

- (NSData *)AES256EncryptWithKey:(NSString *)key;
- (NSData *)AES256DecryptWithKey:(NSString *)key;

@end
