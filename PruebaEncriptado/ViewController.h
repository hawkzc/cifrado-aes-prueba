//
//  ViewController.h
//  PruebaEncriptado
//
//  Created by Adrián Zavala Coria on 17/02/15.
//  Copyright (c) 2015 Horus. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ViewController : UIViewController

@property (weak, nonatomic) IBOutlet UITextField *MessageField;

- (IBAction)EncryptButton:(id)sender;
@end

