//
//  ViewController.m
//  PruebaEncriptado
//
//  Created by Adrián Zavala Coria on 17/02/15.
//  Copyright (c) 2015 Horus. All rights reserved.
//

#import "ViewController.h"
#import "NSData+NSData_AES256_m.h"

@interface ViewController ()

@end

@implementation ViewController
@synthesize MessageField;

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view, typically from a nib.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)EncryptButton:(id)sender {
    [self encriptar:[MessageField text]];
}


- (void)encriptar:(NSString*)text;
{
    //NSString *key = @"este es mi password";
    
    NSString *key = @"-----BEGIN RSA PRIVATE KEY-----"
    "MIIEowIBAAKCAQEAq7P5hXA0cfKc7WH77aBxfQ0K3IkMKnznr4xJmL+PY1KIZBQm"
    "2g8C6EGT2gQSiP9ZgzBOaXAdijiXC3/whn9DA8fZMoUt67wHIoDfBRMIvURH6JDR"
    "vyk9oykX5bgWpWrZ6G8o5n4KuRFlKUu5/iXqR1V1HuC83qNFgQS8zCxDxIrlCe+j"
    "wtoenQTIBCSKE2iYr7+QW3rxBRABDACdjPU4DvWZtTTFixppL/b3P7DGPcwkEX4u"
    "wu4qPhnsIAlpl6cTNo4r402mQNfQEgS2IoWuFWtUy9V9Futfg/6xKanCrZHTSsEe"
    "K/1RQCThrpZuVeLaLtjoauTJbxabgtyi0j/xVwIDAQABAoIBACxE5cN6r3rlg4JM"
    "rgiRQlFsiG1dxE48D/wZ3mvMj+qav+GVtIKqNiStq7IdNK4vr+N8E+tpZaRafM8R"
    "OHFk5GFYKG63GaTawLgro9SKGMm7jhKvK/TubB7iHHA061nQ18fuyQvMWXVnv75K"
    "8PRO0D1+EglZBjkcvXHLR+YYT4yfmr4pkPy8HL5zEzP8SFia7n202uM6ztlErn8e"
    "xUyMUa1f9yUiTPyVI8mfPdIYg7mPiSQ6obkkVG3LkwlM3rQbhBiPyKXhXX18D5/Z"
    "kEgFIFUEPw6F5HtN1yvus0Sasg4VytrQY1WLlPKOlNWE1RwP7q7Mz79azvxIyfYc"
    "QQKfMKkCgYEA24mtnhKqZ2PgZ8aVSkisOEYz4mxRuNrQyxYaf3088bbCPJnyRg4u"
    "ZlGWUnf5fsuEOqfqRRv4Xyfmh84GpNQLUxOs56QcNr1AZ1D4fhqZr6PxfqD8/IsC"
    "FVbJWOv2Ef6Rd5I5m2TguDwkVHogSEKr2a/gsmgt8D1Z+OJuzGVSFQUCgYEAyDh0"
    "VczVrvkHBPEh6tQ3Rud1CRBcUjzzLGNrLdAvFR/zqaF46s00AFeU/1xO4P8tflyY"
    "3iBBU11ZFtOr1ZTlTb92oiPr+sQA5rubFlO+mOqz0SAuMad+5RhZERqwe8ArW010"
    "zC78UcZ7jbQQqQ3CjuKy19jUW05vf+7r0CuQ+6sCgYEAogIA4HTv0WpwEMUIKJBm"
    "nwSXh1DeIedAS45dMZXPIqT2w9frZO1UKmes4eDtejdfthVyGVvhtv7v7WoJlxtv"
    "2/faEfsWYwLYAyZKqmyg22/FZ+gPqaI/gFXSMk+3AGJ3IOEEp/sSpoKjYL/bENg6"
    "j2NuEto0hGQSAEvA7g8bOq0CgYBD0dOeKIcNAK9e/irF92JN8yLSkicjeB/KnlZ0"
    "qzr3hd4sTFS//AgsQX5nzSWu0uDfF5B0VSqriUgfEk33R7swTbVqkI5jX7cKcOvE"
    "cVNzSrTkFmlvSlQSV9/7ijXThTFqeN8iNpwjOv8o7aBjExUe/y0jLgAoeXYNOKnJ"
    "1qgChwKBgCcDwLcQGG01kQzHNLdmr0W1lM8P3WUmI5wf/NCEGBGMHM4sLEKWtHXM"
    "orpnssFz/iOXQfyCkrxXa4Rqhst+Xc0q1RqTezgOYslOhfHlwlk5ednlv1j6+LMf"
    "510ktI2tNSlYOKIHZv1W9jhKblz4PyluQ9tma0s5zzMdFfsHqz08"
    "-----END RSA PRIVATE KEY-----";
    
    NSString *secret = text;
    NSString *public = @"MIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEAq7P5hXA0cfKc7WH77aBx"
    "fQ0K3IkMKnznr4xJmL+PY1KIZBQm2g8C6EGT2gQSiP9ZgzBOaXAdijiXC3/whn9D"
    "A8fZMoUt67wHIoDfBRMIvURH6JDRvyk9oykX5bgWpWrZ6G8o5n4KuRFlKUu5/iXq"
    "R1V1HuC83qNFgQS8zCxDxIrlCe+jwtoenQTIBCSKE2iYr7+QW3rxBRABDACdjPU4"
    "DvWZtTTFixppL/b3P7DGPcwkEX4uwu4qPhnsIAlpl6cTNo4r402mQNfQEgS2IoWu"
    "FWtUy9V9Futfg/6xKanCrZHTSsEeK/1RQCThrpZuVeLaLtjoauTJbxabgtyi0j/x"
    "VwIDAQAB";
    
    NSData *plainPrivado = [secret dataUsingEncoding:NSUTF8StringEncoding];
    NSData *cipherPrivado = [plainPrivado AES256EncryptWithKey:key];
    NSLog(@"Cifrado con clave privada%s\n",[[cipherPrivado description] UTF8String]);
    
    NSData *plainPublico = [secret dataUsingEncoding:NSUTF8StringEncoding];
    NSData *cipherPublico = [plainPublico AES256EncryptWithKey:public];
    
    NSLog(@"Cifrado con clave publica%s\n",[[cipherPublico description] UTF8String]);
    
    
    NSData *plainDecodificarPrivado = [cipherPrivado AES256DecryptWithKey:key];
    NSLog(@"Texto encriptado a decodificar %s\n",[[plainDecodificarPrivado description] UTF8String]);
    NSData *plainDecodificarPublico = [cipherPublico AES256DecryptWithKey:public];
    NSLog(@"Texto encriptado a decodificar %s\n",[[plainDecodificarPublico description] UTF8String]);
    
    NSLog(@"Texto decodificado con clave privada: %s\n",[[[NSString alloc] initWithData:plainDecodificarPrivado encoding:NSUTF8StringEncoding] UTF8String]);
    NSLog(@"Texto decodificado con clave publica: %s\n",[[[NSString alloc] initWithData:plainDecodificarPublico encoding:NSUTF8StringEncoding] UTF8String]);
    
}

@end
