//
//  main.m
//  PruebaEncriptado
//
//  Created by Adrián Zavala Coria on 17/02/15.
//  Copyright (c) 2015 Horus. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

int main(int argc, char * argv[]) {
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
